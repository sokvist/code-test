
(function() {

	var Panels = $('#navigation > dd').hide();
  
	$('#navigation > dt > a').click(function() {
		Panels.slideUp();
		$(this).parent().next().slideDown();
	return false;
	});

}());
